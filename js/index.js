const btnClear = $('#btn-limpar');
const btnAdd = $('#btn-adicionar');
const inputVocab = $('#word');
const listaPalavras = $('#lista-palavras');
const txtVocab = $('#txt-vocab');
const tabelaVocab = $('#tabela-vocabulario');
const inputValidate = $('#validate-analitc');

let collection = [];
let valuesGlobal = 0;
let valueInteration = [0];
let table = [];
let values = [[]];
let cInput = true;

inputVocab.focus( () => {
  inputVocab.removeClass('error');
});

inputVocab.keyup( event =>{
  if(event.key === 'Enter'){
    montarTabela();
  }
});

btnAdd.click(event => {
  montarTabela();
});

btnClear.click(event => {
  inputVocab.val("");
  $('#validate').remove();
  cInput = true;
  txtVocab.html('');
  collection = [];
  let word = collection[event];
  let aux = [];
  collection = [];
  collection = aux;
  aux = [];
  listaPalavras.empty();
  tabelaVocab.children().remove();
  for (i = 0; i < collection.length; i++) {
    let td = criarColunaMeioTabela(`word${i}`, 'list-group-item', collection[i]);
    listaPalavras.append(td);
  }
});

const zerarGlobals = () => {
  values = [[]];
  valuesGlobal = 0;
  valueInteration = [0];
  table = [];
}

const criarEstadoTabela = () => {
  zerarGlobals();
  montarEstado();
  
  table = gerarLinhasTabela();
  tableInsertDados(table);
}

const montarTabela = () => {
  if(cInput){
    let input = criarInput('validate', '', 'Analisar' );
    input.keyup(event => {
      if(table.length > 0){
        validarPalavra();
      }
    });
    inputValidate.append(input);
    cInput = false;
  }
  let value = inputVocab.val().toLowerCase();
  if(value === ""){
    inputVocab.addClass('error');
  } else {
    value = value.split(" ");
    let number = collection.length;
    
    if(value.length > 1){
      for (i = 0; i < value.length; i++) {
        let exists = false;
        number = collection.length;
        if(value[i] !== ""){
          
          for (j = 0; j < collection.length; j++) {
            if(value[i] === collection[j]){
              exists = true;
            }
          }

          if(!exists){
            let td = criarColunaMeioTabela(`word${number}`, 'edit-table-palav', value[i]);
            listaPalavras.append(td);
            collection.push(value[i]);
          }
        }
      }
    } else {
      let exists = false;
      for (j = 0; j < collection.length; j++) {
        if(value[0] === collection[j]){
          exists = true;
        }
      }
      
      if(!exists){
        txtVocab.html('Vocabulário:')
        let td = criarColunaMeioTabela(`word${number}`, 'edit-table-palav', value[0]);
        listaPalavras.append(td);
        collection.push(value[0]);
      }
    }
    inputVocab.val("");
    tabelaVocab.empty();
    criarEstadoTabela();
  }
}

const montarEstado = () => {
  for (let i = 0; i < collection.length; i++) {
    let actualState = 0;
    let word = collection[i];

    for(let j = 0; j < word.length; j++){
      if(typeof values[actualState][word[j]] === 'undefined'){
        let nextState = valuesGlobal + 1;
        values[actualState][word[j]] = nextState;
        values[nextState] = [];
        valuesGlobal = actualState = nextState;
      } else {
        actualState = values[actualState][word[j]];
      }
      if(j == word.length - 1){
        values[actualState]['final'] = true;
      }
    }
  }
}

const gerarLinhasTabela = () =>{
  let vectorvalues = [];
  for (let i = 0; i < values.length; i++) {
    let aux = [];
    aux['estado'] = i;
    let first = 'a';
    let last = 'z';
    for (let j = first.charCodeAt(0); j <= last.charCodeAt(0); j++) {
      let letter = String.fromCharCode(j);
      if(typeof values[i][letter] === 'undefined'){
        aux[letter] = '-'
      } else {
        aux[letter] = values[i][letter]
      }
    }
    if(typeof values[i]['final'] !== 'undefined'){
      aux['final'] = true;
    }
    vectorvalues.push(aux);
  };
  return vectorvalues;
}

const tableInsertDados = vectorvalues => {
  let tableFront = tabelaVocab;
  tableFront.html('');
  let tr =  criarLinhaTabela('', '');
  let th = criarColunaTabela('', '', '');
  tr.append(th);

  let first = 'a';
  let last = 'z';
  for (let j = first.charCodeAt(0); j <= last.charCodeAt(0); j++) {
    th = criarColunaTabela('', '', String.fromCharCode(j));
    tr.append(th);
  }
  tableFront.append(tr);

  for(let i = 0; i < vectorvalues.length; i++){
    let tr = criarLinhaTabela('', `state_${vectorvalues[i]['estado']}`);
    let td = criarColunaMeioTabela('', 'text-td-q', '');
    if(vectorvalues[i]['final']){
      td.html(`q${vectorvalues[i]['estado']}*`);
    } else {
      td.html(`q${vectorvalues[i]['estado']}`);
    }
    tr.append(td);
    let first = 'a';
    let last = 'z';
    
    for (let j = first.charCodeAt(0); j <= last.charCodeAt(0); j++) {
      let letter = String.fromCharCode(j);
      let td = criarColunaMeioTabela('', `letter_${letter}`, '');
      
      if(vectorvalues[i][letter] != '-'){
        td.html(`q${vectorvalues[i][letter]}`);
      } else {
        td.html('-');
      }
      tr.append(td);
    }
    tableFront.append(tr);
  }
}
