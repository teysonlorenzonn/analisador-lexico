
const validarPalavra = () => {
  var words = $('#validate').val().toLowerCase();
  var state = 0;
  if (words.length == 0) {
    $('#validate').removeClass('correct');
    $('#validate').removeClass('error');
    $('#tabela-vocabulario tr').removeClass('selectedStatus');
    $('#tabela-vocabulario td').removeClass('selectedField');
    $(`#tabela-vocabulario .state_${state}`).removeClass('selectedStatusFalse');
    $(`#tabela-vocabulario .letter_${words[i]}`).removeClass('selectedFieldFalse');
  }
  for (var i = 0; i < words.length; i++) {
    if (words[i] >= 'a' && words[i] <= 'z') {
      $('#tabela-vocabulario tr').removeClass('selectedStatus');
      $('#tabela-vocabulario td').removeClass('selectedField');
      $(`#tabela-vocabulario .state_${state}`).addClass('selectedStatus');
      $(`#tabela-vocabulario .letter_${words[i]}`).addClass('selectedField');
      if (table[state][words[i]] != '-') {
        $(`#tabela-vocabulario .state_${state}`).addClass('selectedStatus');
        $(`#tabela-vocabulario .letter_${words[i]}`).addClass('selectedField');
        state = table[state][words[i]];
        $('#validate').addClass('correct');
        $('#validate').removeClass('error');
        $(`#tabela-vocabulario .state_${state}`).removeClass('selectedStatusFalse');
        $(`#tabela-vocabulario .letter_${words[i]}`).removeClass('selectedFieldFalse');
        $(`#tabela-vocabulario .state_${state}`).removeClass('selectedStatusSpace');
        $(`#tabela-vocabulario .letter_${words[i]}`).removeClass('selectedFieldSpace');
      } else {
        $(`#tabela-vocabulario .state_${state}`).addClass('selectedStatusFalse');
        $(`#tabela-vocabulario .letter_${words[i]}`).addClass('selectedFieldFalse');
        $('#validate').removeClass('correct');
        $('#validate').addClass('error');
        break;
      }
    } else if (words[i] == ' ') {
      $('#tabela-vocabulario tr').removeClass('selectedStatus');
      $('#tabela-vocabulario td').removeClass('selectedField');
      $(`#tabela-vocabulario .state_${state}`).addClass('selectedStatusSpace');
      $(`#tabela-vocabulario .letter_${words[i]}`).addClass('selectedFieldSpace');
      if (table[state]['final']) {
        state = 0;
      } else {
        $(`#tabela-vocabulario .state_${state}`).removeClass('selectedStatusSpace');
        $(`#tabela-vocabulario .letter_${words[i]}`).removeClass('selectedFieldSpace');
        $('#validate').removeClass('correct');
        $('#validate').addClass('error');
        break;
      }
    }
  };
}


$("#word").keyup(event => {
  let newValue = validCampVocab(event.target.value.toLowerCase());
  $("#word").val(newValue);
})

$("#validate").keyup(event => {
  let newValue = validCampVocab(event.target.value.toLowerCase());
  $("#validate").val(newValue);
})

const validCampVocab = (value) => {
  for (let i = 0; i < value.length; i++) {
    if (!((value[i] >= 'a' && value[i] <= 'z') || value[i] === ' ')) {
      return value.replace(value[i], '');
    }
  }
  return value;
}
